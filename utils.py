
from matplotlib import pyplot as plt

def imshow(image):
  plt.imshow(image, cmap='gray', vmin = 0, vmax = 255, interpolation='none')

def show_comparison(img1, title1, img2, title2):
  plt.figure(figsize=(25, 25))
  plt.subplot(1, 2, 1)
  plt.title(title1)
  imshow(img1)
  plt.subplot(1, 2, 2)
  plt.title(title2)
  imshow(img2)
